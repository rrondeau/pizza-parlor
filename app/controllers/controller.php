<?php
require('app/models/base_model.php');
require('app/models/customer.php');
require('app/models/pizza.php');

/*
 * Base controller
 */
class Controller {

    #protected $db;

    /**
     * Constructor
     */
    function __construct() { }

    /**
     * Hook after the controller of the route is called
     */
    function afterroute() {
        // Render HTML layout
        echo Template::instance()->render('layout.htm');
    }

}
