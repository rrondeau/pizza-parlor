<?php

/*
 * Controller of orders pages
 */
class Order extends Controller {
    private $errors = array();

    /**
     * Show the form and the orders in the database
     *
     * @object Base 
     */
    function form($f3) {
        $f3->set('form','order_form.htm');
    }

    /**
     * List the pizzas
     *
     * @object Base 
     */
    function show($f3) {
        $pizza_list = Pizza::fetch_all($f3);
        $f3->set('pizza_list', $pizza_list);
    }

    /**
     * Validate and record an order, then show the form and orders in the database
     *
     * @object Base 
     */
    function add($f3) {

        $error = array();
        $this->validate_customer($f3);
        $this->validate_pizza($f3);
        
        if(!count($this->errors)) {
            $objCustomer = new Customer();
            $objCustomer->setName($f3->get('POST.name'));
            $objCustomer->setAddress($f3->get('POST.address'));
            $objCustomer->setPhoneNumber($f3->get('POST.phone_number'));
            $customer_id = $objCustomer->insert();

            $pizzas = $f3->get('POST.pizza');
            foreach ($pizzas as $pizza) {
                $objPizza = new Pizza();
                $objPizza->setToppings($pizza);
                $objPizza->setCustomerId($customer_id);
                $pizza_id = $objPizza->insert();
                unset($objPizza);
            }
            
            $f3->set('messages', array("Order #$customer_id recorded"));
        }
        else {
            $f3->set('errors', $this->errors);
            $f3->set('form','order_form.htm');
        }
    }

   /**
     * Validate and that all the fields of the customer were filled in
     *
     * @object Base 
     */
    private function validate_customer($f3) {
        if (!$f3->exists('POST.name') || !strlen($f3->get('POST.name')) ||
            !$f3->exists('POST.address') || !strlen($f3->get('POST.address')) ||
            !$f3->exists('POST.phone_number') || !strlen($f3->get('POST.phone_number'))) {

            $this->errors[] =  'All the fields are required.';
            return false;
        }
        return true;
    }

   /**
     * Validate that the pizza has at least one topping.
     *
     * @object Base 
     */
    private function validate_pizza($f3) {
        $pizzas = $f3->get('POST.pizza');
        if ($pizzas && is_array($pizzas)) {
            foreach ($pizzas as $pizza) {
                // We require at least one topping per pizza
                if(count($pizza) < 2) {
                    $this->errors[] = 'Each pizza need at least one topping.';  
                    return false;
                }
            }
        }
        return true;
    }
}