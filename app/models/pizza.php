<?php

/**
 * Pizza Model
 */
class Pizza extends Base_Model
{ 
    /**
     * ORM instance of Pizzas
     *
     * @object DB\SQL\Mapper 
     */
    protected $db_pizzas;

    /**
     * Constructor, create the instance of DB\SQL\Mapper on the table pizzas
     */
    function __construct() {
        parent::__construct();
        $this->db_pizzas = new DB\SQL\Mapper($this->db, 'pizzas');
    }

    /**
     * ORM instance of Pizzas
     *
     * @object Base 
     */
    public static function fetch_all($f3) 
    { 
        $db = new DB\SQL($f3->get('DB'));
        // Not ideal, but the ORM provided with the Fatfree do not support joins
        return $db->exec(array('SELECT * FROM pizzas NATURAL JOIN customers ORDER BY posted DESC;'));
    } 
    
    /**
     * Set the toppings in the db_pizzas instance
     *
     * @param array $name The topping choice of the pizza
     */
    public function setToppings($pizza) {
        $this->db_pizzas->topping_1 = (array_key_exists('topping_1', $pizza) ? $pizza['topping_1'] : 0);
        $this->db_pizzas->topping_2 = (array_key_exists('topping_2', $pizza) ? $pizza['topping_2'] : 0);
        $this->db_pizzas->topping_3 = (array_key_exists('topping_3', $pizza) ? $pizza['topping_3'] : 0);
    }

    /**
     * Set the toppings in the db_pizzas instance
     *
     * @param integer $customer_id The id of the customer
     */
    public function setCustomerId($customer_id) {
        $this->db_pizzas->customer_id = $customer_id; 
    }

    /**
     * Insert the customer of db_pizzas in the database
     */
    public function insert()
    {
        $result = $this->db_pizzas->save();
        return $result->get('_id');
    }

} 