<?php

/**
 * Customer Model
 */
class Customer extends Base_Model
{ 
    /**
     * ORM instance of Customers
     *
     * @object DB\SQL\Mapper 
     */
    protected $db_customers;

    /**
     * Constructor, create the instance of DB\SQL\Mapper on the table customers
     */
    function __construct() {
        parent::__construct();
        $this->db_customers = new DB\SQL\Mapper($this->db, 'customers');
    }

    /**
     * Set the name in the db_customer instance
     *
     * @param string $name The name of the customer
     */
    public function setName($name) {
        $this->db_customers->name = $name;
    }

    /**
     * Set the address in the db_customer instance
     *
     * @param string $address The address of the customer
     */
    public function setAddress($address) {
        $this->db_customers->address = $address;

    }

    /**
     * Set the phone number in the db_customer instance
     *
     * @param string $phone_number The phone number of the customer
     */
    public function setPhoneNumber($phone_number) {
        $this->db_customers->phone_number = $phone_number;
    }
 
    /**
     * Insert the customer of db_customer in the database
     */
    public function insert()
    {
        date_default_timezone_set("America/Montreal"); 
        $this->db_customers->posted = date('Y-m-d H:i:s');;
        $result = $this->db_customers->save();
        return $result->get('_id');
    }
} 