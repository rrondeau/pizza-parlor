<?php

/*
 * Base controller
 */
class Base_Model {
    /**
     * Database instance
     *
     * @object DB\SQL 
     */
    protected $db;

    /**
     * Constructor, create the instance of DB\SQL
     */
    function __construct() {

        $f3=Base::instance();

        // Connect to the database
        $db = new DB\SQL($f3->get('DB'));

        // Save frequently used variables
        $this->db = $db;

    }
}
