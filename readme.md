# Task Description

You will be writing a small app for a small pizza parlour. This pizza parlour has 1 pizza with three optional toppings. It's very sad.

Design two database tables. One is a customer table. One is a pizza table, you can use a column for each ingredient. Each pizza order generates a record in the pizza table, tied to a customer.

Next, design and implement a PHP program to capture pizza orders. Your solution will be evaluated on both the quality of the design and the quality of the implementation. List any frameworks that you use.


# Framework used

Fat-Free Framework :  http://fatfreeframework.com/


# Installation instruction #

## 1 . Extract or clone the the project.


## 2 . Configure your host  file and virtual host file to the folder you extracted the project.

### hosts file  
```
#!bash hosts

127.0.0.1 pizza.local
```

### virtual host file  

```
#!bash

<VirtualHost *:80>
        DocumentRoot "/path/to/project/"
        ServerName pizza.dev
        <Directory "/path/to/project/">
                AllowOverride All
                Order allow,deny
                Allow from all
        </Directory>
</VirtualHost>
```

## 3 . Restart Apache

## 4 . Give your web user ownership to the whole project.

###Exemple Mac
```
#!bash 

sudo chown -R  _www /path/to/project/
```

###Exemple Unix
```
#!bash 

sudo chown -R  www-data /path/to/project/
```

# Description of the folders
```
./
./app                 Application folder, contain custom code of the project    
./app/controllers     List of controllers which generate the page
./app/models          List of models to fetch and alter the database
./app/views           List of views and template
./app/routes.ini      File containing the route of the site
./config              Configuration of the project
./lib                 Library provided by fat-free CMS
./tmp                 Folder containing parsed template file
./db                  Folder containing the sqlite3 database
./ui                  Folder containing the static assets
./index.php           Bootstrap of the project
./dump.sql            Structure of the database
./composer.json 
./readme.md           This file
```
