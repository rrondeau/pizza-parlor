function addRow() {
    var table = document.getElementById("pizza_table");
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
    var colCount = table.rows[1].cells.length;

    for(var i=0; i<colCount; i++) {
        var newcell = row.insertCell(i);
        newcell.innerHTML = table.rows[1].cells[i].innerHTML;
        newcell.className =  'center';
        
        if(i == 0) {
            newcell.childNodes[0].setAttribute("name", "pizza["+(rowCount-1)+"][id]");
            newcell.childNodes[0].value = rowCount-1;
        }
        else {
            newcell.childNodes[0].setAttribute("name", "pizza["+(rowCount-1)+"][topping_"+i+"]");
            newcell.childNodes[0].checked = false;
        }

    }
}
function deleteRow() {
    try {
        var table = document.getElementById("pizza_table");
        var rowCount = table.rows.length;
        if(rowCount > 2) {
            table.deleteRow(rowCount-1);
        }
        else {
            alert("Cannot delete all the rows.");
        }
    }catch(e) {
        alert(e);
    }
}