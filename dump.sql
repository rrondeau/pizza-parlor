DROP TABLE IF EXISTS "customers";
CREATE TABLE "customers" (
  "customer_id" integer NULL PRIMARY KEY AUTOINCREMENT,
  "name" text NOT NULL,
  "address" text NOT NULL,
  "phone_number" text NOT NULL,
  "posted" text NULL
);


DROP TABLE IF EXISTS "pizzas";
CREATE TABLE "pizzas" (
  "pizza_id" integer NULL PRIMARY KEY AUTOINCREMENT,
  "topping_1" integer NULL DEFAULT '0',
  "topping_2" integer NULL DEFAULT '0',
  "topping_3" integer NULL DEFAULT '0',
  "customer_id" integer NOT NULL,
  FOREIGN KEY ("customer_id") REFERENCES "customers" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);
